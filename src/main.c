#include "mem_internals.h"
#include "mem.h"
#include "util.h"
#include <stdint.h>


static inline struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}


bool test1() {
    printf("Первый тест\n");
    void* a = _malloc(600);
    if (a == NULL) {
        printf("Тест завален\n");
        return false;
    } else {
        printf("Первый тест завершен успешно, память выделена\n\n");
        _free(a);
        return true;
    }
}

bool test2(){
    printf("Второй тест\n");
    void* a = _malloc(150);
    void* b = _malloc(150);
    void* c = _malloc(150);
    if (a == NULL || b == NULL || c == NULL) {
        printf("Ошибка, не выделилась память\n");
        return false;
    }else {
        _free(a);
        if (!(block_get_header(a)->is_free)) {
            printf("Первый блок не освободился");
            return false;
        }
        printf("Освободили первый блок");
        _free(b);
        _free(c);
        printf("Успех\n\n");
        return true;
    }
}

bool test3(){
    printf("Третий тест\n");
    void* a = _malloc(150);
    void* b = _malloc(150);
    void* c = _malloc(150);
    if (a == NULL || b == NULL || c == NULL) {
        printf("Ошибка, не выделилась память\n");
        return false;
    }else {
        _free(a);
        _free(b);
        if (!block_get_header(a)->is_free || !block_get_header(b)->is_free) {
            err("Первый или второй блок не освободился");
            return false;
        }
        printf("Освободили первые два блока");
        _free(c);
        printf("Успех\n\n");
        return true;
    }
}

bool test4(){
    printf("Четвертый тест\n");
    void* a = _malloc(5000);
    struct block_header* ha = block_get_header(a);

    void* next = ha->contents + ha->capacity.bytes;
    void* b = _malloc(5000);
    if (a == NULL || b == NULL) {
        printf("Ошибка, не выделилась память\n");
        return false;
    }
    struct block_header* hb = block_get_header(b);

    if ((void*) hb == next){
        printf("Новый регион памяти расширяет старый\n");
        _free(a);
        _free(b);
        return true;
    } else {
        printf("Тест 4 не пройден");
        return false;
    }
}

bool test5(){
    printf("Тест 5 начат - Память закончилась, выделение памяти в другом месте.\n");
    void* a = _malloc(10000);
    struct block_header* ha = block_get_header(a);

    struct block_header* block_i = ha;
    while (block_i->next != NULL){
        block_i = block_i->next;
    }
    void* addr_after = block_i->contents + block_i->capacity.bytes;
    void* new_mem = mmap(addr_after, 2000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);


    void* b = _malloc(7000);
    struct block_header* hb = block_get_header(b);

    if (ha->is_free || hb->is_free || (block_i->next == new_mem)){
        printf("Тест завален");
        return false;
    }
    _free(a);
    _free(b);
    printf("Тест 5 завершён - новый регион памяти успешно выделен в другом месте.\n");
    return true;
}



int main (int argc, char** argv){
    heap_init(2000);
    if (argc == 0){
        if (test1() && test2() && test3() && test4() && test5()) {
            printf("Все тесты пройдены");
        } else {
            printf("Какой-то из тестов не прошёл");
        }
    } else {
        if ((argv[1][0] < '1') || (argv[1][0]) > '5') {
            printf("Ошибка, чтобы запустить определенный тест введите число от 1 до 5");
        } else {
            switch (argv[1][0]) {
                case '1':
                    test1();
                    break;
                case '2':
                    test2();
                    break;
                case '3':
                    test3();
                    break;
                case '4':
                    test4();
                    break;
                case '5':
                    test5();
                    break;
            }
        }
    }
}
